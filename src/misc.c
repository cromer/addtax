#include <stdbool.h>
#include <unistd.h>
#include <ctype.h>
#include <stdlib.h>

bool is_float(const char *string) {
	if (string == NULL) {
		return false;
	}
	char *endptr;
	strtod(string, &endptr);
	if (string == endptr) {
		return false;
	}
	while (isspace((unsigned char ) *endptr)) {
		endptr++;
	}
	return *endptr == '\0';
}
