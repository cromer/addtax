#include <gtk/gtk.h>
#include <math.h>
#include <libintl.h>
#include <locale.h>
#include <stdbool.h>
#include <getopt.h>
#include "include/addtax.h"
#include "include/misc.h"

GtkLabel *calculated_value = NULL;

void calculate_tax(GtkWidget *entry) {
	char *entry_text;
	float money;
	const gchar *label_text;
	entry_text = (char*) gtk_entry_get_text(GTK_ENTRY(entry));
	money = atof(entry_text);
	if (is_float(entry_text)) {
		g_print("$%d * %f = ", (int) round(money), TAX);
		money = money * TAX;
		g_print("$%d\n", (int) round(money));
		label_text = g_strconcat((const gchar *) "$", g_strdup_printf("%i", (int) round(money)), NULL);
		gtk_label_set_text(calculated_value, label_text);
	}
}

int main(int argc, char *argv[]) {
	int opt;
	float money;
	int long_index = 0;
	static struct option long_options[] = {
		{"version",		no_argument,	0,	'v'},
		{0,				0,				0,	0}
	};
	
	setlocale(LC_ALL, "");
	bindtextdomain("addtax", "/usr/share/locale/");
	textdomain("addtax");
	
	if (argc > 1) {
		// Process options
		while ((opt = getopt_long(argc, argv,"v", long_options, &long_index)) != -1) {
			switch (opt) {
				case 'v':
					printf("%s %s\n", argv[0], VERSION);
					break;
				default:
					fprintf(stderr, _("Usage: %s [prices...]\n"), argv[0]);
					return 1;
			}
		}
		
		// Process arguments that are not options
		while (optind < argc) {
			money = atof(argv[optind]);
			if (is_float(argv[optind++])) {
				printf("$%d * %f = ", (int) round(money), TAX);
				money = money * TAX;
				printf("$%d\n", (int) round(money));
			}
			else {
				fprintf(stderr, _("Usage: %s [prices...]\n"), argv[0]);
				return 1;
			}
		}
		return 0;
	}

	GtkBuilder *builder;
	GtkWidget *window;
	GError *error = NULL;

	gtk_init(&argc, &argv);

	builder = gtk_builder_new();
	gtk_builder_add_from_resource(builder, "/cl/cromer/addtax/window_main.glade", &error);
	if (error != NULL) {
		fprintf(stderr, "Error: %s\n", error->message);
		return 1;
	}

	window = GTK_WIDGET(gtk_builder_get_object(builder, "window_main"));
	gtk_builder_connect_signals(builder, NULL);

	calculated_value = (GtkLabel*) gtk_builder_get_object (builder, "calculated_value");

	g_object_unref(builder);

	gtk_widget_show(window);
	gtk_main();

	return 0;
}

// Called when calculate is clicked
void on_button_calculate_clicked(__attribute__((unused)) GtkWidget *button, GtkWidget *entry) {
	calculate_tax(entry);
}

// Called when the user presses enter on the entry
void on_entry_price_activate(GtkWidget *entry) {
	calculate_tax(entry);
}

// Called when window is closed
void on_window_main_destroy() {
	gtk_main_quit();
}
